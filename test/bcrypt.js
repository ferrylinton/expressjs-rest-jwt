'use strict';
const bcrypt = require('bcryptjs');

var plain = "password";
var hash = bcrypt.hashSync(plain, 10);

console.log(plain);
console.log(hash);
console.log(bcrypt.compareSync(plain, hash));
console.log(bcrypt.compareSync("111111", hash));