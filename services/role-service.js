const pageUtil = require('../util/page-util');
const auditLogService = require("../services/audit-log-service");
const Sequelize = require('sequelize');
const NotFoundError = require('../error/not-found-error');
const { ROLE } = require("../config/constant").ActionModel;
const { CREATE, UPDATE, DELETE } = require("../config/constant").ActionType;
const { Op } = require('sequelize');
const { sequelize, Authority, Role } = require('../models/index');


const includeAuthorities = [
  {
    model: Authority,
    as: 'authorities',
    attributes: {
      exclude: []
    },
    through: {
      attributes: []
    }
  }
]

async function findAndCountAll(req) {
  let params = pageUtil.getParams(req);
  params.include = includeAuthorities;
  params.distinct = true;
  params.col = "id";
  params.order = [
    ['name', 'ASC'],
  ]

  if (params.keyword) {
    let keyword = "%" + params.keyword.toLowerCase() + "%";

    params.where = {
      [Op.or]: [
        Sequelize.where(Sequelize.fn('lower', Sequelize.col('Role.name')), { [Op.like]: keyword }),
        Sequelize.where(Sequelize.fn('lower', Sequelize.col('Role.created_by')), { [Op.like]: keyword }),
        Sequelize.where(Sequelize.fn('lower', Sequelize.col('Role.updated_by')), { [Op.like]: keyword }),
      ]
    }
  }

  let data = await Role.findAndCountAll(params);
  let page = pageUtil.getPage(req, data.count);
  return { data, page };
}

async function findOne(req, t) {
  let role = await Role.findOne({
    include: includeAuthorities,
    where: { id: req.params.id },
    transaction: t
  });

  if (role) {
    return role;
  } else {
    throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
  }
}

async function create(req) {
  try {
    let { name } = req.body;
    let authorities = await getAuthorities(req.body.authorityIds);

    let result = await sequelize.transaction(async (t) => {
      let options = { user: req.user, transaction: t };
      let role = await Role.create({ name }, options);
      await role.setAuthorities(authorities, options);

      role.dataValues.authorities = authorities;
      return role.dataValues;
    });

    await auditLogService.log(ROLE, CREATE, true, result, req);
    return result;
  } catch (err) {
    await auditLogService.log(ROLE, CREATE, false, err, req);
    throw err;
  }
}

async function update(req) {
  try {
    let authorities = await getAuthorities(req.body.authorityIds);
    let role = await findOne(req);

    let result = await sequelize.transaction(async (t) => {
      let options = { user: req.user, transaction: t };
      role.name = req.body.name;

      await role.save(options);
      await role.setAuthorities(authorities, options);

      role.dataValues.authorities = authorities;
      return role.dataValues;
    });

    await auditLogService.log(ROLE, UPDATE, true, result, req);
    return result;
  } catch (err) {
    await auditLogService.log(ROLE, UPDATE, false, err, req);
    throw err;
  }
}

async function destroy(req) {
  try {
    let role = await sequelize.transaction(async (t) => {
      let role = await findOne(req, t);
      await role.setAuthorities([], { user: req.user, transaction: t });
      await role.destroy({ transaction: t });

      return role;
    });

    await auditLogService.log(ROLE, DELETE, true, role, req);
  } catch (err) {
    await auditLogService.log(ROLE, DELETE, false, err, req);
    throw err;
  }
}

async function isNameExist(name, id) {
  let role = await Role.findOne({
    where: { name }
  });

  if (role) {
    if (!(id && (id == authority.id))) {
      return Promise.reject(`${name} is already exist`);
    }
  }
}

async function getAuthorities(ids) {
  return await Authority.findAll({
    where: {
      id: {
        [Op.in]: ids
      }
    }
  });
}

module.exports = {
  findAndCountAll,
  findOne,
  create,
  update,
  destroy,
  isNameExist
};

