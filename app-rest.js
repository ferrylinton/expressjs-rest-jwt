const createError = require('http-errors');
const express = require('express');
const jwt = require('express-jwt');
const fs = require('fs');
const morgan = require('morgan');
const logger = require('./config/winston');

const { RevokedToken } = require('./models/index');

const publicKey = fs.readFileSync('./keys/dev/public.pub', 'utf8');
const securityRestRouter = require('./routes/rest/security-rest-route');
const authorityRestRouter = require('./routes/rest/authority-rest-route');
const roleRestRouter = require('./routes/rest/role-rest-route');
const userRestRouter = require('./routes/rest/user-rest-route');

var app = express();

var isRevokedCallback = async function (req, payload, done) {
  try {
    let token = await RevokedToken.findOne({
      where: { id: payload.jti }
    });

    if (token) {
      return done(null, true);
    } else {
      return done(null, false);
    }

  } catch (error) {
    return done(error)
  }
};

var checkJwt = jwt({
  secret: publicKey,
  isRevoked: isRevokedCallback,
  algorithms: ['RS256'],
}).unless({ path: ['/api/auth'] });

app.use(morgan('combined', { stream: logger.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(checkJwt);

app.use('/', securityRestRouter);
app.use('/authorities', authorityRestRouter);
app.use('/roles', roleRestRouter);
app.use('/users', userRestRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  logger.error("######### REST API ERROR #########");
  logger.error(err.stack);

  if (err.errors) {
    let messages = [];

    err.errors.forEach(function (element, index) {
      if (element.constructor.name === 'ValidationErrorItem') {
        messages.push({
          param: element.path,
          msg: element.message,
          value: element.value
        });
      }
    });

    res.status(400);
    res.json(messages);
  } else {
    res.status(err.status || 500);
    res.json({ "msg": err.message });
  }

});

module.exports = app;
