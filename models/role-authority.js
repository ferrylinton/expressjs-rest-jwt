"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {

  class RoleAuthority extends Model {

    static associate(models) {
      // define association here
    }

  };

  RoleAuthority.init({

    roleId: {
      type: DataTypes.STRING(11),
      references: {
        model: 'role',
        key: 'id'
      }
    },

    authorityId: {
      type: DataTypes.STRING(11),
      references: {
        model: 'authority',
        key: 'id'
      }
    },

    createdAt: {
      type: DataTypes.DATE
    },

    createdBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    }

  }, {
    sequelize,
    modelName: 'RoleAuthority',
    tableName: 'sec_role_authority',
    hooks: {

      beforeBulkCreate: (instances, options) => {
        console.log("####################################### beforeBulkCreate");

        instances.forEach(function (instance) {
          instance.createdAt = new Date();
          instance.createdBy = options.user.id + "," + options.user.username;
        });

      }

    }
  });

  return RoleAuthority;

};