function getValidationErrors(errors, res) {
    return res.status(400).json(errors.array().map(error => {
        return {
            "msg": error.msg,
            "param": error.param
        }
    }));
}

module.exports = {
    getValidationErrors
};