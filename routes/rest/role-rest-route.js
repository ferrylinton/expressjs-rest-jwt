const roleService = require("../../services/role-service");
const express = require("express");
const { check, validationResult } = require("express-validator");

var router = express.Router();
var validate = [
  check("name", "name is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().matches(/^[A-Za-z0-9]+$/).withMessage("must contains alphanumeric")
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50")
    .bail().custom((value, { req }) => { return roleService.isNameExist(value, req.params.id) })
]

router.get('/', findAndCountAll);
router.get('/:id', findOne);
router.post('/', validate, create);
router.put('/:id', validate, update);
router.delete('/:id', destroy);

async function findAndCountAll(req, res, next) {
  try {
    var result = await roleService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findOne(req, res, next) {
  try {
    const role = await roleService.findOne(req);
    res.status(200).json(role);
  } catch (error) {
    return next(error)
  }
}

async function create(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const role = await roleService.create(req);
      res.status(201).json(role);
    }
  } catch (error) {
    return next(error)
  }
}

async function update(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const role = await roleService.update(req);
      res.status(200).json(role);
    }
  } catch (error) {
    return next(error)
  }
}

async function destroy(req, res, next) {
  try {
    await roleService.destroy(req);
    res.status(204).send();
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
