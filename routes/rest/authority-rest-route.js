const authorityService = require("../../services/authority-service");
const express = require("express");
const { check, validationResult } = require("express-validator");

var guard = require('express-jwt-permissions')()
var router = express.Router();
var validate = [
  check("name", "name is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().matches(/^[A-Za-z0-9_-]+$/).withMessage("must contains alphanumeric, -, or _")
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50")
    .bail().custom((value, { req }) => { return authorityService.isNameExist(value, req.params.id) })
]

router.get("/", guard.check('AUTHORITY_VIEW'), findAndCountAll);
router.get("/:id", guard.check('AUTHORITY_VIEW'), findOne);
router.post("/", guard.check('AUTHORITY_MODIFY'), validate, create);
router.put("/:id", guard.check('AUTHORITY_MODIFY'), validate, update);
router.delete("/:id", guard.check('AUTHORITY_MODIFY'), destroy);

async function findAndCountAll(req, res, next) {
  try {
    let result = await authorityService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findOne(req, res, next) {
  try {
    let authority = await authorityService.findOne(req);
    res.status(200).json(authority);
  } catch (error) {
    return next(error)
  }
}

async function create(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      let authority = await authorityService.create(req);
      res.status(201).json(authority);
    }
  } catch (error) {
    return next(error)
  }
}

async function update(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      let authority = await authorityService.update(req);
      res.status(200).json(authority);
    }
  } catch (error) {
    return next(error)
  }
}

async function destroy(req, res, next) {
  try {
    await authorityService.destroy(req);
    res.status(204).send();
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
