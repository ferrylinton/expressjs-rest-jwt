var express = require('express');
var { body, validationResult } = require('express-validator');
var db = require('../models/index');
var { Op } = require("sequelize");
var securityService = require('../services/security-service');
var router = express.Router();
var logger = require('../config/winston');
var path = require('path');

var LOGIN_VALIDATION = [
  body('username').isLength({ min: 3 }),
  body('password').isLength({ min: 3 })
]

router.get('/login', async (req, res, next) => {
  try {
    let data = {
      layout: false,
      title: 'Login',
      locale: req.getLocale()
    };

    // info: test message my string {}
    logger.log('info', 'test message %s', 'my string');
    
    // info: test message 123 {}
    logger.log('info', 'test message %d', 123);
    
    // info: test message first second {number: 123}
    logger.log('info', 'test message %s, %s', 'first', 'second', { file: path.basename(__filename) });

    res.render('login', data);
  } catch (error) {
    return next(error)
  }
});

router.post('/login', LOGIN_VALIDATION, async (req, res, next) => {
  try {
    let data = {
      layout: false,
      title: 'Login',
      locale: req.getLocale()
    };

    let username = req.body.username;
    let password = req.body.password;
    let rememberMe = req.body.rememberMe !== undefined;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      data.errors = errors.errors;
      res.render('login', data);
    } else {
      let user = await db.user.findOne({
        where: {
          "username": {
            [Op.eq]: username
          }
        }
      });

      if (user != null) {
        if (securityService.compare(password, user.password_hash)) {
          req.session.loggedin = true;
          res.redirect('/');
        }else{
          data.message = "wrongUsernameOrPassword";
          res.render('login', data);
        }
      }else{
        data.message = "wrongUsernameOrPassword";
        res.render('login', data);
      }
    }
  } catch (error) {
    return next(error)
  }
});

router.post('/logout', async (req, res, next) => {
  try {
    req.session.loggedin = null;
    res.redirect('/login');
  } catch (error) {
    return next(error)
  }
});

module.exports = router;
