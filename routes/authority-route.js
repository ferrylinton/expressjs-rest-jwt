var authorityService = require('../services/authority-service');
var express = require('express');
var router = express.Router();

router.get('/', findAndCountAll);

async function findAndCountAll(req, res, next){
  try {
    var result = await authorityService.findAndCountAll(req);
    res.render('index', result);
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
